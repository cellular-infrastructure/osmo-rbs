#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

#include <dahdi/user.h>

#include <osmocom/core/utils.h>

/* this program wants to be called with 'strace -s1024 -x' so you see the data
 * betewen DAHDI and it */

uint8_t sabme_frame_nonraw[] = { 0xFA, 0x7D, 0x7F, 0x00, 0x00 };

static int fd;
static uint8_t dummy_buf[8192];

static const struct value_string dahdi_evt_names[] = {
	{ DAHDI_EVENT_NONE,		"NONE" },
	{ DAHDI_EVENT_ALARM,		"ALARM" },
	{ DAHDI_EVENT_NOALARM,		"NOALARM" },
	{ DAHDI_EVENT_ABORT,		"HDLC ABORT" },
	{ DAHDI_EVENT_OVERRUN,		"HDLC OVERRUN" },
	{ DAHDI_EVENT_BADFCS,		"HDLC BAD FCS" },
	{ DAHDI_EVENT_REMOVED,		"REMOVED" },
	{ 0, NULL }
};

static void handle_dahdi_exception(int fd)
{
	int rc, evt;

	rc = ioctl(fd, DAHDI_GETEVENT, &evt);
	if (rc < 0)
		return;

	printf("DAHDI EVENT %s\n", get_value_string(dahdi_evt_names, evt));
}

static void dummy_read(int fd, unsigned int len)
{
	int rc;

	if (len > sizeof(dummy_buf))
		len = sizeof(dummy_buf);

	rc = read(fd, dummy_buf, len);
	if (rc == -1)
		handle_dahdi_exception(fd);
	else if (rc > 0)
		printf("Received %u bytes (%s)\n", rc, osmo_hexdump(dummy_buf, rc));
}

int main(int argc, char **argv)
{
	int rc;
	uint8_t tei = 62, sapi = 62;
	int one = 1;

	if (argc < 2)
		exit(2);

	fd = open(argv[1], O_RDWR|O_NONBLOCK);
	if (fd < 0) {
		perror("open");
		exit(1);
	}
	rc = ioctl(fd, DAHDI_HDLCFCSMODE, &one);
	if (rc < 0)
		printf("unable to set channel into HDLC/FCS mode ?!?\n");

	if (argc >= 3)
		tei = atoi(argv[2]);
	if (argc >= 4)
		sapi = atoi(argv[3]);

	printf("dev = %s, TEI = %u, SAPI = %u\n", argv[1], tei, sapi);
	sabme_frame_nonraw[0] = (sapi << 2) | 2;
	sabme_frame_nonraw[1] = (tei << 1) | 1;

	while (1) {
		fd_set read_fd;
		struct timeval timeout;

		FD_ZERO(&read_fd);
		FD_SET(fd, &read_fd);

		timeout.tv_sec = 0;
		timeout.tv_usec = 300000;

		rc = select(fd+1, &read_fd, NULL, NULL, &timeout);
		if (rc < 0) {
			perror("Select");
			exit(1);
		} else if (rc) {
			dummy_read(fd, 16);
		} else {
			rc = write(fd, sabme_frame_nonraw, sizeof(sabme_frame_nonraw));
			if (rc == -1)
				handle_dahdi_exception(fd);
		}
	}
}
