#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

/* this program wants to be called with 'strace -s1024 -x' so you see the data
 * betewen DAHDI and it */

const uint8_t aa_frame[] = { 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
			   0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa };

const uint8_t ff_frame[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
			   0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };


//const uint8_t sabme_frame[] = { 0xFA, 0x7D, 0x7F };

const uint8_t sabme_frame[] = { 0x7e, 0x7e, 0x7e, 0x5f, 0x5f, 0x3e, 0xce, 0x49, 0xef, 0xcf, 0xcf, 0xcf, 0xcf, 0xcf };

static int fd;
static uint8_t dummy_buf[8192];

static void dummy_read(int fd, unsigned int len)
{
	int rc;

	if (len > sizeof(dummy_buf))
		len = sizeof(dummy_buf);

	rc = read(fd, dummy_buf, len);
}

/* try to re-create the initialization routine as from a trace between Racal 6113 and RBS */
static void init(void)
{
	int i;
	uint8_t fiftyfive = 0x55;

	/* first play some 0xaa sequences */
	for (i = 1; i < 50; i++) {
		write(fd, aa_frame, sizeof(aa_frame));
		dummy_read(fd, sizeof(aa_frame));
	}
	/* 0xaa sequence with final 0x55 */
	write(fd, aa_frame, sizeof(aa_frame)-1);
	write(fd, &fiftyfive, 1);
	dummy_read(fd, sizeof(aa_frame));

	/* play some 0xff sequence */
	for (i = 0x92f; i < 0x960; i++) {
		write(fd, ff_frame, sizeof(aa_frame));
		dummy_read(fd, sizeof(aa_frame));
	}

	/* send a SABME frame */
	write(fd, sabme_frame, sizeof(sabme_frame));

	/* endless loop, read data. */
	while (1)
		dummy_read(fd, sizeof(sabme_frame));
}

int main(int argc, char **argv)
{
	if (argc < 2)
		exit(2);

	fd = open(argv[1], O_RDWR);
	if (fd < 0) {
		perror("open");
		exit(1);
	}

	init();
}
